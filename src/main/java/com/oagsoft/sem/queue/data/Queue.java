package com.oagsoft.sem.queue.data;

import java.util.LinkedList;
import java.util.List;

public class Queue<T> {
    private List<T> q;
    private boolean modified;
    private final int maxSize;
    private final boolean limited;

    public Queue(final int maxSize) {
        modified = false;
        this.maxSize = maxSize;
        limited = this.maxSize > 0;
        System.out.println(limited + " " + this.maxSize + "  " + maxSize);
        q = new LinkedList<T>();
    }

    public synchronized boolean isModified() {
        return modified;
    }

    public synchronized final List<T> getElements() {
        return q;
    }

    public synchronized void push(T e) {
        modified = true;
        q.add(e);
        if (limited && q.size() >= maxSize) {
            q.remove(0);
        }
    }

    public synchronized T pop() {
        modified = true;
        return q.remove(0);
    }

    public synchronized void update() {
        modified = false;
    }

    @Override
    public synchronized String toString() {
        return "S: " + modified + " L: " + limited + " V: " + q;
    }
}
