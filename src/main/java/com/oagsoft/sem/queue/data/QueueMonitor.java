package com.oagsoft.sem.queue.data;

import java.util.function.Consumer;

public class QueueMonitor<T> extends Thread {
    private Queue<T> queue;
    private Consumer<Queue<T>> consumer;

    public QueueMonitor(Queue<T> queue) {
        this.queue = queue;
    }

    public QueueMonitor<T> consumer(final Consumer<Queue<T>> consumer) {
        this.consumer = consumer;
        return this;
    }

    @Override
    public void run() {
        while (true) {
            try {
                if (queue.isModified()) {
                    consumer.accept(queue);
                    queue.update();
                }
                Thread.sleep(10);
            } catch (InterruptedException e) {
            }
        }
    }

}
