package com.oagsoft.sem.queue.file;

import com.oagsoft.sem.queue.data.Queue;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class FileWriter<T extends Queue> implements Consumer<T> {
    private final String fileName;

    public FileWriter(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public void accept(T t) {
        try {
            List<String> lines = (List<String>) t.getElements().stream()
                    .map(Objects::toString)
                    .collect(Collectors.toList());
            Files.write(Paths.get(fileName), lines);
        } catch (IOException e) {
        }
    }
}
