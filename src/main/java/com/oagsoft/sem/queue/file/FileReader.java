package com.oagsoft.sem.queue.file;

import com.oagsoft.sem.queue.data.Queue;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.function.Function;

public class FileReader<V, T extends Queue<V>> implements Function<String, T> {
    private final int maxSize;

    public FileReader(final int maxSize) {
        this.maxSize = maxSize;
    }

    @Override
    public T apply(String fileName) {
        Queue<V> queue = new Queue<>(maxSize);
        try {
            Files.readAllLines(Paths.get(fileName))
                    .stream().map(this::fun)//TODO: Borrar esta linea despues
                    .forEach(e -> queue.push((V) e));
        } catch (IOException e) {
        }
        return (T) queue;
    }

    // TODO: Esta funcion se debe borrar. Es solo para que se vea que valores pasan por aqui.
    private String fun(String e) {
        System.out.println(e);
        return e;
    }
}
