package com.oagsoft.sem.queue.handler;

import com.oagsoft.sem.queue.data.Queue;
import com.oagsoft.sem.queue.data.QueueMonitor;
import com.oagsoft.sem.queue.file.FileWriter;
import com.oagsoft.sem.queue.file.FileReader;

public class QueueHandler<T> {
    private Queue<T> queue;
    private QueueMonitor<T> queueMonitor;

    public QueueHandler(final int maxSize, String fileName) {
        queue = new FileReader<T, Queue<T>>(maxSize).apply(fileName);
        queueMonitor = new QueueMonitor<T>(queue).consumer(new FileWriter(fileName));
        queueMonitor.start();
    }

    public void addElement(T v) {
        queue.push(v);
    }

    @Override
    public String toString() {
        return queue.toString();
    }
}
