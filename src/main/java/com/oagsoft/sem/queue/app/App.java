package com.oagsoft.sem.queue.app;

import com.oagsoft.sem.queue.handler.QueueHandler;

import java.util.Random;

public class App extends Thread {
    public static final int MAX = 100; // Con MAX en cero la cola no tiene limite.
    public static final String FILE_NAME = "prueba.txt";
    QueueHandler<Integer> queueHandler;

    public App() {
        queueHandler = new QueueHandler<>(MAX, FILE_NAME);
    }

    public void run() {
        while (true) {
            try {
                // Este codigo seria el que se coloca donde se crean los eventos
                if (new Random().nextDouble() > 0.9) {
                    queueHandler.addElement(new Random().nextInt(10));
                    System.out.println(queueHandler.toString());
                }
                Thread.sleep(100);
            } catch (InterruptedException e) {
            }
        }
    }

    public static void main(String[] args) {
        new App().start();
    }
}
